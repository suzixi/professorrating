from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
class IindexTest(unittest.TestCase):
    def setUp(self):
        chromedriver="/Users/apple/Desktop/delete/chromedriver"
        self.browser=webdriver.Chrome(chromedriver)
        self.browser.implicitly_wait(2)

    def tearDown(self):
        self.browser.quit()

    def test_get_index(self):
        self.browser.get("http://localhost:8000")
        self.assertIn('Index',self.browser.title)
        h1text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('HOMEPAGE',h1text)
        #self.fail('Test finished')
    def test_retrieve_list(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Backlog',self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Backlog',header_text)
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('this is a backlog')
        inputbox.send_keys(Keys.ENTER)
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_element_by_tag_name('tr')
        self.assertTrue(any(row.text=='this is a back log' for row in rows),'not appear in table')

if __name__=='main':
    unittest.main()