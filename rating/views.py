from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

def home_page(request):
    if request.method == 'POST':
        return HttpResponse(request.POST['item_content'])
    return render(request,'index.html')