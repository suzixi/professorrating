from django.test import TestCase
from django.core.urlresolvers import resolve
from rating.views import home_page
from django.http import HttpRequest
from django.shortcuts import 

class HomePageTest(TestCase):
    def test_homepage_resolve(self):
        found=resolve('/')
        self.assertEqual(found.func,home_page)
    def test_index_content(self):
        request=HttpRequest()
        response=home_page(request)
        self.assertTrue(response.content.startswith(b'<!DOCTYPE html>'))
        self.assertIn(b'Index',response.content)
        self.assertTrue(response.content.endswith(b'</html>'))
    def test_index_save_post(self):
        request = HttpRequest()
        request.method='POST'
        request.POST['item_content'] = 'a sample backlog'

        response = home_page(request)
        self.assertIn('a sample backlog',response.content.decode())
        expected_html = render_to_string